﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A2_BigSchool_sinhtodau.Startup))]
namespace A2_BigSchool_sinhtodau
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
